*** Settings ***
Library           RPA.Browser

*** Keywords ***
My Set Up Time
    Set Selenium Implicit Wait    10 seconds
    Set Selenium Timeout    20 seconds
    Set Selenium Speed    0.7 second

My Tat Pop Up
    Wait Until Keyword Succeeds    120    5    Execute Javascript    document.querySelector("#onesignal-slidedown-cancel-button").click()

My Tat LiveChat
    Select Frame    css=#_hjRemoteVarsFrame
    Select Frame    css=#chat-widget
    ${livechat}    Run Keyword And Return Status    Wait Until Element Is Visible    css=#widget-global-8x4lsr5wp2 > div > div > div > div.lc-urm3d7.e1ohfhv0 > div:nth-child(3) > button
    Run Keyword If    '${livechat}' == 'TRUE'    Click Element    css=#widget-global-8x4lsr5wp2 > div > div > div > div.lc-urm3d7.e1ohfhv0 > div:nth-child(3) > button
