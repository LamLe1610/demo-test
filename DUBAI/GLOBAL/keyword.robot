*** Settings ***
Library           RPA.Browser
Library           RPA.HTTP
Library           OperatingSystem
Library           DateTime
Resource          ../Global/Variable.robot
Library           RPA.Excel.Files

*** Variables ***

*** Keywords ***
Setup_Template
    Open Browser    ${null}    chrome
    Set_Up_Timing

Set_Up_Timing
    Set Selenium Implicit Wait    10 seconds
    Set Selenium Timeout    20 seconds
    Set Selenium Speed    1 second
    Maximize Browser Window

Register
    Open Chrome Browser    https://debet.com/
    Tat_Pop_Up
    Set_Up_Timing
    Click Element If Visible    css=.button--red
    ${date}    Get Current Date    result_format=%m%d%H%M%S
    Input Text When Element Is Visible    css=[placeholder='Nhập từ 6 đến 30 ký tự']    Lam${date}
    Input Text When Element Is Visible    css=[placeholder='Nhập từ 6 ký tự trở lên']    lamkuteo
    Input Text When Element Is Visible    css=[placeholder='Xác nhận lại mật khẩu']    lamkuteo
    Input Password    css=[placeholder='Độ dài 10 số, VD: 0974321845']    09526165655
    Click Element If Visible    css=div.flex-column > .btn-submit
    Wait Until Element Is Visible    css=a#bank-tab span
    [Teardown]    Tear_Down

Login
    Open Chrome Browser    https://debet.com/
    Set_Up_Timing
    Tat_Pop_Up
    Input Text When Element Is Visible    css=[placeholder='Tên tài khoản']    testauto07
    Input Text When Element Is Visible    css=.input--type-5[placeholder='Mật khẩu']    Aa123456
    Click Element If Visible    css=.button--login

Template
    [Arguments]    ${URL}    ${TextĐN}    ${TextP}    ${Đn}    ${Sport}    ${SSport}
    Go to    ${URL}
    True/False
    Input Text When Element Is Visible    ${TextĐN}    testervn1997
    Input Text When Element Is Visible    ${TextP}    lamkuteo
    Click Element If Visible    ${Đn}
    Click Element If Visible    ${Sport}
    Click Element If Visible    ${SSport}

Tro_choi_Alive
    Click Element If Visible    css=nav.header__menu li:nth-of-type(6) > a
    Click Element If Visible    ${TroChoi_ChauA}[0]
    Switch Window    new
    Wait Until Element Is Visible    css=#GameCanvas
    Close Window
    Switch Window    main
    Click Element If Visible    ${TroChoi_ChauA}[1]
    Switch Window    new
    Wait Until Element Is Visible    css=#GameCanvas

Tat_Pop_Up
    Wait Until Element Is Visible    css=#onesignal-slidedown-dialog
    Click Element If Visible    css=#onesignal-slidedown-cancel-button

Loop_NoHu
    Click Element If Visible    css=nav.header__menu li:nth-of-type(6) > a
    FOR    ${i}    IN    @{list_nohu}
        Wait Until Keyword Succeeds    20s    3s    Execute Javascript    document.querySelector("#slotGameSlider2 > div.swiper-wrapper > div:nth-child(${i}) > div > a > button").click()
        Check_Request_URL
    END

Check_Request_URL
    Switch Window    new
    Get Title
    ${url}    Get Location
    Create Session    lam    ${url}
    ${request}    Get Request    lam    ${EMPTY}    allow_redirects=FALSE
    Log To Console    ${request}
    Close Window
    Switch Window    main

True/False
    ${boo}    Run Keyword And Return Status    Wait Until Element Is Visible    css=#onesignal-slidedown-dialog
    Run Keyword If    ${boo}==True    Click Element If Visible    css=#onesignal-slidedown-cancel-button

Get_Date
    ${date}    Get Current Date    result_format=%m%d%H%M%S
    Comment    Set Global Variable    ${date}

Random_Number
    ${Random Numbers}    Evaluate    random.sample(range(00000000000, 99999999999), 1)
    Comment    Set Global Variable    ${Random Numbers}

Tear_Down
    Run Keyword If    '${TEST_STATUS}' == 'FAIL'    Report
    Close All Browsers

Report
    ${date}    Get Current Date    result_format=%d/%m-%H/%M/%S
    Create File    ${File_Report}
    ${URL}    Get Location
    ${title}    Get Title
    Append To File    ${File_Report}    ${title}
    Append To File    ${File_Report}    FAILED\n
    Append To File    ${File_Report}    ${date}\n
    Append To File    ${File_Report}    ${URL}\n

Run_On_Table
    [Arguments]    ${x}
    Go To    ${x}[Domain]
    Wait Until Page Contains    ${x}[Text]

Loop_Domain
    Open Workbook    C:\\Users\\Dell\\Documents\\bong.xlsx
    ${read}    Read Worksheet As Table    name=List    header=true
    Close Workbook
    FOR    ${x}    IN    @{read}
        Run Keyword And Continue On Failure    Run_On_Table    ${x}
    END
    Close All Browsers
    [Return]    ${read}
