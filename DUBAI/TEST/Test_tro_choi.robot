*** Settings ***
Resource          ../Global/keyword.robot

*** Test Cases ***
Đăng ký
    Register
    [Teardown]    Tear_Down

TroChoi
    [Setup]    Login
    Tro_choi_Alive
    [Teardown]    Tear_Down

TroChoi_Nohu
    [Setup]    Login
    Loop_NoHu
    [Teardown]    Tear_Down

Template_Lam
    [Setup]    Setup_Template
    [Template]    Template
    #URL    #TextĐN    #TextP    #NutĐN    #NutSport    #NutSSport
    https://www.zbet.win/    css=[placeholder='Tên đăng nhập...']    css=[placeholder='Mật khẩu...']    css=.btn--head-sign-in    css=.menu-primary__link--sport    css=a[href='https://www.zbet.win/sport/a-sport/play']
    https://debet.com/    css=[placeholder='Tên tài khoản']    css=.input--type-5[placeholder='Mật khẩu']    css=.button--login    css=a[href='/sport']    css=[src='//d1.debet.com/rs2/images/lobby/sport/sport_chaua.png']
    [Teardown]    Tear_Down
