*** Settings ***
Resource          Variables.robot
Library           RPA.Browser
Library           RPA.Excel.Files
Library           RPA.HTTP
Library           OperatingSystem
Library           ImageLibrary
Library           DateTime

*** Keywords ***
My Read File
    #Đọc dữ liệu từ file excel
    Open Workbook    ${LinkFile}
    ${SoDong}    Read Worksheet As Table    name=List Domain    header=True
    Close Workbook
    Log To Console    ${SoDong}
    #Get request và trả status
    FOR    ${x}    IN    @{SoDong}
        ${sta}    Run Keyword And Return Status    Http Get    ${x}[Domain]
        ${loca}    Get Location
        Run Keyword If    '${sta}'=='False'    Run Keywords    Append To File    ${path}    ${loca}\nThis site can't be reached\r\n
        ...    AND    Continue For Loop
        Run Keyword And Continue On Failure    My Check Status    ${x}
    END
    [Return]    ${SoDong}

My Run On Table
    My Check Status

My Check Broken Image
    ${we_ele}    Get WebElement    //img [@src]
    ${ele}    Get Element Attribute    ${we_ele}    src
    ${re}    Http Get    ${ele}
    ${url}    Get Location
    ${time}    Get Current Date    result_format=%m/%d-%H/%M/%S
    Run Keyword If    "${re.status_code}"=="404"    Append To File    ${path}    ${url}\nBroken Layout\r\n

My Check Status
    [Arguments]    ${x}
    Go To    ${x}[Domain]
    Check Page Contain
    Check Broken Image
                                Web server is down    Trong thời gian bảo trì mọi dịch vụ của chúng tôi sẽ tạm ngưng.
                                ${loca}\n${y}\r\n
                                ...    AND    Fail

My Suite Setup
    Create File    ${path}
    ${date}    Get Current Date    result_format=%H:%M:%S, %d/%m/%y.
    Append To File    ${path}    ${date}\r\n

My Set Up
    Open Browser    ${null}    Chrome
    Comment    Set Selenium Implicit Wait
    Comment    Set Selenium Timeout    0
    Comment    Set Selenium Speed    0
    Maximize Browser Window

Check Element
    ${true}    Run Keyword And Return Status    Page Should Contain    ${file}
    Run Keyword If    "${true}"=="TRUE"    Append To File    ${path}    Failed
    ${ele}    Get WebElement    //link [@rel="stylesheet" and @href]
    ${attr}    Get Element Attribute    ${ele}    href
    ${re}    Http Get    ${attr}
    ${url}    Get Location
    Run Keyword If    "${re.status_code}"=="404"    Append To File    ${path}    ${url}\nBroken Layout\r\n

Check Page Contain
    #Nội dung của các page bị chặn
    ${file}    Create List    Domain not found    Access denied    Not found    FREE Whois Privacy    Web server is down    Trong thời gian bảo trì mọi dịch vụ của chúng tôi sẽ tạm ngưng.
    FOR    ${y}    IN    @{file}
        ${err_content}    Run Keyword And Return Status    Page Should Contain    ${y}
        ${loca}    Get Location
        Run Keyword If    '${err_content}'=='True'    Run Keywords    Append To File    ${path}    ${loca}\n${y}\r\n
        ...    AND    Fail
    END

Check Broken Image
    #Hình ảnh ở các trang có bị bể không
    ${we_ele}    Get WebElements    //img[@src]
    FOR    ${z}    IN RANGE    1    4
        ${ele}    Get Element Attribute    ${we_ele}[${z}]    src
        ${re}    Http Get    ${ele}
        ${url}    Get Location
        ${time}    Get Current Date    result_format=%m/%d-%H/%M/%S
        Run Keyword If    "${re.status_code}"=="404"    Append To File    ${path}
    END
